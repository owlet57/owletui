## OwletUI

---

Owlet UI is a minimalist, easy to use and customize skin for Rainmeter.  It works, by default, on Rainmeter version 2.4 and above, on computers running Windows Vista or higher (due to a default dependency on Segoe UI Light for its font).  It's offered on Github for quick and easy visibility of the code I'm using, as well as an insight into the changes I'm making, but it's also offered on deviantArt as a good indictator of stability.

Don't care about stability? Github will be hosting experimental versions of the skin, and I'm encouraging development alongside me.  Help me fix bugs and add features if you want!

---

### Features:

* Simple launcher, prepared for the edges of your screen
* Simple, and all in one system monitors for CPU, RAM, and Swap
* Up to 4 Disk Monitors.
* Sleek, stylish type
* Easily customizable (just follow the directions)

---

### Installation:

#### Automatic:

1. Download and launch the OwletUI .rmskin file
2. Follow the instructions on screen
3. Done!

#### Manual (required for beta):

1. Download the code as a zip file from Github.
2. Install to your Rainmeter skins folder (default: C:\Users\ [your username] \Documents\Rainmeter\Skins)
3. Done!

---

Editing the launcher, fonts, and colors:

1. Install OwletUI (manually, or automatically)
2. Open the Rainmeter skins folder (default: C:\Users\ [your username] \Documents\Rainmeter\Skins), and open Owlet UI\@resources
3. Open variables.inc in your text editor of choice.
4. Edit any needed variables, and then refresh all skins.
5. Done!

---